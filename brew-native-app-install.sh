#!/usr/bin/env bash

# Install native apps

brew install --cask brave-browser
brew install --cask todoist
brew install --cask iterm2
brew install --cask google-chrome
brew install --cask google-drive
brew install --cask flux
brew install --cask appcleaner
brew install --cask moom
brew install --cask visual-studio-code
brew install --cask android-studio
brew install --cask rancher
brew install --cask figma
brew install --cask postman
brew install --cask slack
brew install --cask discord
brew install --cask spotify
brew install --cask vlc
brew install --cask betterdisplay
# brew install --cask the-unarchiver
# brew cask install licecap

# cleanup
brew cleanup
