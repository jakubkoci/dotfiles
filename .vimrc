" Enable syntax highlighting
syntax on

" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent

" set $ cursor when changing word
set cpoptions+=$
 
" Show line numbers
set number
